#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import os
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

__all__ = ['BaseBot']

class BaseBot:
    def __init__(self, TOKEN = None):
        self.TOKEN = TOKEN or os.environ.get("TG_API_TOKEN")
        assert (self.TOKEN), TypeError("missing 1 required positional arguments: TOKEN")
        self.updater = Updater(self.TOKEN)

    def init_handlers(self):
        for handler in self.handlres:
            self.updater.dispatcher.add_handler(handler)
        self.updater.dispatcher.add_error_handler(self.error_handler)
    
    def error_handler(self, bot, update, error):
        """Log Errors caused by Updates."""
        logger.warning('Update "%s" caused error "%s"', update, error)

    def heroku(self, APPNAME, PORT):
        # Start the Bot
        self.updater.start_polling()
        # Run the bot until the user presses Ctrl-C or the process receives SIINT,
        self.updater.start_webhook(listen="0.0.0.0", port=int(PORT), url_path=self.TOKEN)
        self.updater.bot.set_webhook(f"https://{appname}.herokuapp.com/{self.TOKEN}")
        self.updater.idle()
        
    def cli(self):
        # Start the Bot
        self.updater.start_polling()
        # Run the bot until the user presses Ctrl-C or the process receives SIGINT,
        # SIGTERM or SIGABRT
        self.updater.idle()


    def start(self):
        self.init_handlers()
        herokuapp = os.environ.get('HEROKU_APP_NAME', False)
        if herokuapp:
            port = os.environ.get('PORT')
            self.heroku(herokuapp, port)
        else:
            self.cli()