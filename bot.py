#!/usr/bin/env python
# -*- coding: utf-8 -*-
from BaseBot import BaseBot
import logging
import os
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, MessageEntity, InlineQueryResultArticle, InputTextMessageContent
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, InlineQueryHandler, ChosenInlineResultHandler, MessageHandler, Filters
import pastebin
from uuid import uuid4

class Paster(pastebin.api):
    def __init__(self):
        super().__init__()
    def parse(self, paste_query):
        pass

class PasteBot(BaseBot): 
    def __init__(self, TOKEN):
        self.handlres = [
                CommandHandler("start", self.comm_start, pass_args=True, pass_user_data=True),
                InlineQueryHandler(self.il_paster, pass_user_data=True, pattern=None),
                ChosenInlineResultHandler(self.cb_chosen_il, pass_user_data=True)
            ]
        super().__init__(TOKEN = TOKEN)

    def comm_start(self, bot, update, user_data, args):
        msg = str(
f"""שלום {update.message.from_user.first_name}!
באמצעותי תוכל להדביק בקלות פוסטים באתר Pastebin.com
"""
        )
        update.message.reply_text(msg)

    def il_paster(self, bot, update, user_data):
        il = update.inline_query
        keyboard = InlineKeyboardMarkup(
            [
                [InlineKeyboardButton("♻️ רענן ♻️", callback_data=f"refresh?user_id={il.from_user.id}")]
            ]
        )
        results = [
            InlineQueryResultArticle(
                id=uuid4(),
                title="Pastebin",
                input_message_content=InputTextMessageContent(
                    "הקישור יתעדכן תיכף..."
                ),
                reply_markup = keyboard
            )
        ]
        il.answer(results)
    
    def cb_chosen_il(self, bot, update, user_data):
        ch = update.chosen_inline_result
        post_link = Paster().paste(ch.query)
        msg = "✅ הפוסט הועלה בהצלחה. ✅"+"\n"+f"{post_link}"
        keyboard = InlineKeyboardMarkup(
            [
                [InlineKeyboardButton("מעבר לפוסט", url = post_link)]
            ]
        )
        bot.edit_message_text(msg, inline_message_id = ch.inline_message_id, reply_markup = keyboard)


    def comm_help(self, bot, update):
        update.message.reply_text("help")

if __name__ == "__main__":
    token = os.environ.get("TG_TOKEN")
    bot = PasteBot(token)
    bot.start()