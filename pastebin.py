import requests
import os

AllPasteBinExcpetions = [
    "Bad API request, invalid api_option",
    "Bad API request, invalid api_dev_key",
    "Bad API request, IP blocked",
    "Bad API request, maximum number of 25 unlisted pastes for your free account",
    "Bad API request, maximum number of 10 private pastes for your free account",
    "Bad API request, api_paste_code was empty",
    "Bad API request, maximum paste file size exceeded",
    "Bad API request, invalid api_expire_date",
    "Bad API request, invalid api_paste_private",
    "Bad API request, invalid api_paste_format",
    "Bad API request, invalid api_user_key",
    "Bad API request, invalid or expired api_user_key",
    "Bad API request, use POST request, not GET",
    "Bad API request, invalid login",
    "Bad API request, account not active",
    "Bad API request, invalid POST parameters"
]

class PastebinException(Exception):
    def __init__(self, error):
        self.msg = error.split(",")[-1]
    
class api:
    def __init__(self, API_DEV_KEY = None):
        self.API_DEV_KEY = API_DEV_KEY or os.environ.get("API_DEV_KEY")
        self.session = requests.session()
        self.user_key = None
        self.api_endpoint = "https://pastebin.com/api/"

    def _except(self, response):
        if response.text in AllPasteBinExcpetions:
            raise PastebinException(response.text)
            
    def _post(self, method, params = {}, **kwargs):
        params['api_dev_key'] = self.API_DEV_KEY
        if self.user_key: params['api_user_key'] = self.user_key
        res =  self.session.post(f"{self.api_endpoint}/{method}.php", data=params, **kwargs)
        self._except(res)
        return res.text

    def paste(self, paste, private = 0, expire_date = "N", paste_name = None, paste_format = None):
        data = {
            "api_option": "paste",
            "api_paste_code": paste,
            "api_paste_private": private,
            "expire_date": expire_date,
            "api_paste_name": paste_name,
            "api_paste_format": paste_format
        }
        return self._post("api_post", data)

    def login(self, username, password):
        data = {
            "api_user_name": username,
            "api_user_password": password
        }
        self.user_key = self._post("api_login", data)
        return True
    
    
        